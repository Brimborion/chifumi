/**
 * Package which contains all classes for testing Chifumi project.
 */
package com.me.chifumi;

import org.junit.Assert;
import org.junit.Test;

import com.me.chifumi.Game;
import com.me.chifumi.Item;


/**
 * Main tests for Chifumi project. Mostly tests the resolveDuel method.
 */
public class ChifumiTest {
	/**
	 * Test when player have a rock moove and bot a scissors one.
	 */
	@Test
	public void testRockVsScissors() {
		Game game = new Game();
		Item rock = Item.ROCK;
		Item scissors = Item.SCISSORS;
		String result = game.resolveDuel(rock, scissors);
		
		Assert.assertEquals("gagne.", result);
	}
	/**
	 * Test when player have a rock moove and bot a paper one.
	 */
	@Test
	public void testRockVsPaper() {
		Game game = new Game();
		Item rock = Item.ROCK;
		Item paper = Item.PAPER;
		String result = game.resolveDuel(rock, paper);
		
		Assert.assertEquals("perd.", result);
	}
	/**
	 * Test when player have a scissors moove and bot a rock one.
	 */
	@Test
	public void testScissorsVsRock() {
		Game game = new Game();
		Item rock = Item.ROCK;
		Item scissors = Item.SCISSORS;
		String result = game.resolveDuel(scissors, rock);
		
		Assert.assertEquals("perd.", result);
	}
	/**
	 * Test when player have a scissors moove and bot a paper one.
	 */
	@Test
	public void testScissorsVsPaper() {
		Game game = new Game();
		Item paper = Item.PAPER;
		Item scissors = Item.SCISSORS;
		String result = game.resolveDuel(scissors, paper);
		
		Assert.assertEquals("gagne.", result);
	}
	/**
	 * Test when player have a paper moove and bot a rock one.
	 */
	@Test
	public void testPaperVsRock() {
		Game game = new Game();
		Item rock = Item.ROCK;
		Item paper = Item.PAPER;
		String result = game.resolveDuel(paper, rock);
		
		Assert.assertEquals("gagne.", result);
	}
	/**
	 * Test when player have a paper moove and bot a scissors one.
	 */
	@Test
	public void testPaperVsScissors() {
		Game game = new Game();
		Item paper = Item.PAPER;
		Item scissors = Item.SCISSORS;
		String result = game.resolveDuel(paper, scissors);
		
		Assert.assertEquals("perd.", result);
	}
	/**
	 * Test when player and bot have the same moove.
	 */
	@Test
	public void testSameItemDuel() {
		Game game = new Game();
		Item item = Item.ROCK;
		String result = game.resolveDuel(item, item);
		
		Assert.assertEquals("est à égalité.", result);
	}
}

/**
 * Package which contains all classes for Chifumi project.
 */
package com.me.chifumi;

/**
 * Enum of Item used in Chifumi game.
 */
public enum Item {
	/** Represent the rock item with some attributes.*/
	ROCK		(1, "rock", "R"),
	/** Represent the scissors item with some attributes.*/
	SCISSORS	(2, "scissors", "S"),
	/** Represent the paper item with some attributes.*/
	PAPER		(3, "paper", "P");
	/** Represent the number of items. */
	public static final int NUMBEROFITEM = 3;
	/** Id of the item. */
	private int itemId;
	/** Name of the item. */
	private String itemName;
	/** Letter of the item. */
	private String itemLetter;
	/**
	 * Item constructor.
	 * @param tmpItemId define the item id.
	 * @param tmpItemName define the common item name.
	 * @param tmpItemLetter define the item shortcut.
	 */
	private Item(int tmpItemId, String tmpItemName, String tmpItemLetter) {
		this.itemId = tmpItemId;
		this.itemName = tmpItemName;
		this.itemLetter = tmpItemLetter;
	}
	/**
	 * Return an item from his id.
	 * @param itemId the Id of the item.
	 * @return an item.
	 */
	public static Item getItemById(int itemId) {
		Item item = null;
		for (Item type : Item.values()) {
			if (itemId == type.itemId) {
				item = type;
				break;
			}
		}
		return item;
	}
	/**
	 * Return an item from his shortcut.
	 * @param itemLetter the shortcut of the item.
	 * @return an item.
	 */
	public static Item getItemByLetter(String itemLetter) {
		Item item = null;
		if (itemLetter != null) {
			for (Item type : Item.values()) {
				if (itemLetter.equals(type.itemLetter)) {
					item = type;
					break;
				}
			}
		}
		return item;
	}
	/**
	 * Return the id of the item.
	 * @return the id of the item.
	 */
	public int getId() {
		return this.itemId;
	}
	/**
	 * Return the name of the item.
	 * @return the name of the item.
	 */
	public String getName() {
		return this.itemName;
	}
	/**
	 * Return the letter of the item.
	 * @return the letter of the item.
	 */
	public String getLetter() {
		return this.itemLetter;
	}
}

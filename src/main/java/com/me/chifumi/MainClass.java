/**
 * Package which contains all classes for Chifumi project.
 */
package com.me.chifumi;

/**
 * Main class to launch the app.
 */
public final class MainClass {
	/**
	 * Default start method.
	 * @param args are beautiful arguments.
	 */
	public static void main(String[] args) {
		MainClass main = new MainClass();
		main.init();
	}
	/**
	 * Initialization method.
	 */
	public void init() {
		Game game = new Game();
		game.displayGame();
	}

}

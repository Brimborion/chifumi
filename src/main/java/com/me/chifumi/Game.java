/**
 * Package which contains all classes for Chifumi project.
 */
package com.me.chifumi;

import java.util.Random;
import java.util.Scanner;

/**
 * The main class for the gaming needs.
 */
public class Game {
	/** The bot item during the game. */
	private Item botItem;
	/** The player item during the game. */
	private Item playerItem;
	
	/**
	 * Main constructor for the Game class.
	 */
	public Game() {
	}
	/**
	 * The game launcher.
	 */
	public void displayGame() {
		Scanner scanner = new Scanner(System.in);
		this.playerItem = null;
		this.botItem = getRandomItem();
		String playerItemLetter;
		String duelResult;
		while (this.playerItem == null) {
			System.out.println("Entrez R pour pierre, P pour feuille ou S "
					+ "pour ciseaux : ");
			playerItemLetter = scanner.nextLine();
			this.playerItem = Item.getItemByLetter(playerItemLetter);
		}
		scanner.close();
		duelResult = resolveDuel(this.playerItem, this.botItem);
		System.out.println("Joueur : " + this.playerItem.getName()
				+ "\nBot : " + this.botItem.getName()
				+ "\nJoueur " + duelResult);
	}
	/**
	 * Solve the duel between the player and the bot.
	 * @param player is the player item.
	 * @param bot is the bot item.
	 * @return an item.
	 */
	public String resolveDuel(Item player, Item bot) {
		String duelResult;
		if (player == bot) {
			duelResult = "est à égalité.";
		} else if ((bot.getId() == (player.getId() + 1))
				|| (player.getId() == Item.NUMBEROFITEM && bot.getId() == 1)) {
			duelResult = "gagne.";
		} else {
			duelResult = "perd.";
		}
		return duelResult;
	}
	/**
	 * Return an item randomly between paper, scissors and rock.
	 * @return an item.
	 */
	public Item getRandomItem() {
		Random random = new Random();
		int itemId;
		itemId = random.nextInt(Item.NUMBEROFITEM) + 1;
		Item randomItem = Item.getItemById(itemId);
		return randomItem;
	}
}

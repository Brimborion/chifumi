# Chifumi #

Projet de DL (semaine 06) dans le cadre du cours de qualité du code.

### Technos/outils employées ###

* Java
* Eclipse
* JUnit
* Ant

### Différents points abordés ###

* Utilisation/personnalisation d'Eclipse
* Tests unitaires
* Bonnes pratiques Java
    * Organisation du code
    * Organisation des dossiers
    * Nomenclatures
    * etc.
* Utilisation de Ant
* Utilisation de checkstyle